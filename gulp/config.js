module.exports = {
  src : {
    root    : 'src/',
    jade    : 'src/jade',
    emails  : 'src/emails/',
    sass    : 'src/sass/',
    js      : 'src/js/',
    img     : 'src/img/',
    helpers : 'gulp/helpers/'
  },
  dest:{
    root    : 'build/',
    css     : 'build/css/',
    html    : 'build/',
    emails  : 'build/emails/',
    js      : 'build/js/',
    img     : 'build/img/'
  },
};