//=include mark.js
$(document).ready(function() {

	var Toggle = {
		constructor: function(selector) {
			this.$btn = $(selector);
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$btn.on("click", this._toggleTarget.bind(this));
		},
		_toggleTarget: function(event) {
			var $el = $(event.currentTarget);
			var $target = $("."+$el.data("toggle"));
			if ($el.data("toggle") !== "parent") {
				$target.toggleClass("is-active");
				$el.toggleClass("is-active");
			}
			else {
				$el.parents(".js-parent").toggleClass("is-active");
				$el.toggleClass("is-active");
			}
			return false;
		}
	}
	Toggle.constructor(".js-toggle");
	
	$(".js-nav-overlay").on("click", function() {
		$(this).removeClass("is-active");
		$(".js-toggle").removeClass("is-active");
		$(".js-nav-panel").removeClass("is-active");
	});
	$('.js-masonry').masonry({
		itemSelector: '.js-masonry-item',
		columnWidth: '.js-masonry-sizer',
		gutter: 0,
		percentPosition: true
	});

	$(".js-mark").each(function() {
		var mark = $(this).data("mark");
		$(this).append(pieChart(mark, 25));
	});
	$(".js-btn-info").on("click", function(){
		$(".js-info").addClass("is-active");
	});
	$(".js-close-info").on("click", function(){
		$(".js-info").removeClass("is-active");
		$('body,html').animate({scrollTop:0},800)
	});

	var Window = {
		init: function() {
			this.$el = $(".js-window");
			this.$btnClose = $(".js-close-window");
			this.$btnOpen = $(".js-open-window");
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$btnClose.on("click", this._hideWindow.bind(this));
			this.$btnOpen.on("click", this._openWindow.bind(this));
		},
		_hideWindow: function(event) {
			this.$el.removeClass("is-active");
			return false;
		},
		_openWindow: function(event) {
			var $btn = $(event.currentTarget);
			this.$window = $($btn.data("window"));
			this.index = ""+$btn.data("slide");
			this.$window.addClass("is-active");
			if (this.index) {
				this._scrollToSlide(this.$window, this.index)
			}
			return false;
		},
		_scrollToSlide: function(target, index) {
			target.find(".slick-slider").slick("slickGoTo", index);
		}
	}
	Window.init();
	var inputFile = {
		init() {
			this._$input = $(".js-file input");
			this._$inputFileName = $(".js-file-name");
			this._$inputFileSize = $(".js-file-size");
			this._bindEvents();
		},
		_bindEvents: function() {
			this._$input.on("change", this._getFile.bind(this));
		},
		_getFile: function() {
			var $input = $(event.target);
			console.log($input);
			var fileName = $input[0]["files"][0]["name"];
			var fileSize = $input[0]["files"][0]["size"]/1000000;
			fileSize = Math.floor(fileSize*100)/100;
			$(".js-file").addClass("is-active");
			if (fileSize > 5) {
				this._$inputFileSize.text("Размер файла превышает 5 Mb!");
			}
			else {
				this._$inputFileSize.text(fileSize+" Mb");
				this._$inputFileName.text(fileName);
			}
		}
	}
	inputFile.init();
});