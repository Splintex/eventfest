//=include ../../bower/picturefill/dist/picturefill.js
//=include ../../bower/jquery-validation/dist/jquery.validate.js
//=include ../../bower/slick-carousel/slick/slick.js


$(document).ready(function () {


	$('.js-validate').each(function() {
		$(this).validate({
			debug: true,
			errorClass: 'is-error',
			rules: {
				email: {
					email: true
				},
				
			},
			messages: {
			    required: "Required input",
			    email: "Укажите правильный e-mail"
			}

		});
	});

	var formNav = {
		init: function() {
			this._$btn = $(".js-change-form");
			this._$form = $("[data-form]");
			this._bindEvents();
		},
		_bindEvents: function() {
			this._$btn.on("click", this._changeForm.bind(this));
		},
		_changeForm: function() {
			var $btn = $(event.currentTarget);
			var $target = $('[data-form="'+$btn.data("target")+'"]');
			this._$form.addClass("is-hidden");
			$target.removeClass("is-hidden");
		}
	}
	formNav.init();

	$("[data-slick]").slick();
		


});