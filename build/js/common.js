function pieChart(mark, size, color) {
	percentage = mark*100/5;
    var svgns = "http://www.w3.org/2000/svg";
    var chart = document.createElementNS(svgns, "svg:svg");
    chart.setAttribute("width", size);
    chart.setAttribute("height", size);
    chart.setAttribute("viewBox", "0 0 " + size + " " + size);
    // Background circle
    var back = document.createElementNS(svgns, "circle");
    back.setAttributeNS(null, "cx", size / 2);
    back.setAttributeNS(null, "cy", size / 2);
    back.setAttributeNS(null, "r",  size / 2);
    var color = "#fff";
    back.setAttributeNS(null, "fill", color);
    chart.appendChild(back);
    // primary wedge
	
    var path = document.createElementNS(svgns, "path");
    var unit = (Math.PI *2) / 100;    
    var startangle = 0;
    var endangle = percentage * unit - 0.001;
    var x1 = (size / 2) + (size / 2) * Math.sin(startangle);
    var y1 = (size / 2) - (size / 2) * Math.cos(startangle);
    var x2 = (size / 2) + (size / 2) * Math.sin(endangle);
    var y2 = (size / 2) - (size / 2) * Math.cos(endangle);
    var big = 0;
    if (endangle - startangle > Math.PI) {
        big = 1;
    }
    var d = "M " + (size / 2) + "," + (size / 2) +  // Start at circle center
        " L " + x1 + "," + y1 +     // Draw line to (x1,y1)
        " A " + (size / 2) + "," + (size / 2) +       // Draw an arc of radius r
        " 0 " + big + " 1 " +       // Arc details...
        x2 + "," + y2 +             // Arc goes to to (x2,y2)
        " Z";                       // Close path back to (cx,cy)
    path.setAttribute("d", d); // Set this path 
    path.setAttribute("fill", '#ee3b37');

    chart.appendChild(path); // Add wedge to chart
    // foreground circle
    var front = document.createElementNS(svgns, "circle");
    front.setAttributeNS(null, "cx", (size / 2));
    front.setAttributeNS(null, "cy", (size / 2));
    front.setAttributeNS(null, "r",  (size * 0.42)); //about 34% as big as back circle 
    front.setAttributeNS(null, "fill", "#fff");
    chart.appendChild(front);
    return chart;
}

$(document).ready(function() {

	var Toggle = {
		constructor: function(selector) {
			this.$btn = $(selector);
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$btn.on("click", this._toggleTarget.bind(this));
		},
		_toggleTarget: function(event) {
			var $el = $(event.currentTarget);
			var $target = $("."+$el.data("toggle"));
			if ($el.data("toggle") !== "parent") {
				$target.toggleClass("is-active");
				$el.toggleClass("is-active");
			}
			else {
				$el.parents(".js-parent").toggleClass("is-active");
				$el.toggleClass("is-active");
			}
			return false;
		}
	}
	Toggle.constructor(".js-toggle");
	
	$(".js-nav-overlay").on("click", function() {
		$(this).removeClass("is-active");
		$(".js-toggle").removeClass("is-active");
		$(".js-nav-panel").removeClass("is-active");
	});
	$('.js-masonry').masonry({
		itemSelector: '.js-masonry-item',
		columnWidth: '.js-masonry-sizer',
		gutter: 0,
		percentPosition: true
	});

	$(".js-mark").each(function() {
		var mark = $(this).data("mark");
		$(this).append(pieChart(mark, 25));
	});
	$(".js-btn-info").on("click", function(){
		$(".js-info").addClass("is-active");
	});
	$(".js-close-info").on("click", function(){
		$(".js-info").removeClass("is-active");
		$('body,html').animate({scrollTop:0},800)
	});

	var Window = {
		init: function() {
			this.$el = $(".js-window");
			this.$btnClose = $(".js-close-window");
			this.$btnOpen = $(".js-open-window");
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$btnClose.on("click", this._hideWindow.bind(this));
			this.$btnOpen.on("click", this._openWindow.bind(this));
		},
		_hideWindow: function(event) {
			this.$el.removeClass("is-active");
			return false;
		},
		_openWindow: function(event) {
			var $btn = $(event.currentTarget);
			this.$window = $($btn.data("window"));
			this.index = ""+$btn.data("slide");
			this.$window.addClass("is-active");
			if (this.index) {
				this._scrollToSlide(this.$window, this.index)
			}
			return false;
		},
		_scrollToSlide: function(target, index) {
			target.find(".slick-slider").slick("slickGoTo", index);
		}
	}
	Window.init();
	var inputFile = {
		init() {
			this._$input = $(".js-file input");
			this._$inputFileName = $(".js-file-name");
			this._$inputFileSize = $(".js-file-size");
			this._bindEvents();
		},
		_bindEvents: function() {
			this._$input.on("change", this._getFile.bind(this));
		},
		_getFile: function() {
			var $input = $(event.target);
			console.log($input);
			var fileName = $input[0]["files"][0]["name"];
			var fileSize = $input[0]["files"][0]["size"]/1000000;
			fileSize = Math.floor(fileSize*100)/100;
			$(".js-file").addClass("is-active");
			if (fileSize > 5) {
				this._$inputFileSize.text("Размер файла превышает 5 Mb!");
			}
			else {
				this._$inputFileSize.text(fileSize+" Mb");
				this._$inputFileName.text(fileName);
			}
		}
	}
	inputFile.init();
});