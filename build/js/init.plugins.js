$(document).ready(function() {

	"use strict";

	document.createElement( "picture" );

	//Waves.attach('.js-waves', ['waves-button', 'waves-float']);
	//Waves.attach('.js-waves-light', ['waves-button', 'waves-float', 'waves-light']);


	$(".js-select").each(function() {
		if ($(this).attr("placeholder")) {
			$(this).select2({
				minimumResultsForSearch: Infinity,
				placeholder: $(this).attr("placeholder")
			});
		}
		else {
			$(this).select2({
				minimumResultsForSearch: Infinity
			});
		}
		
	});

	$(".js-banner-slider").slick({
		dots: false,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		swipe: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	$(".js-box-slider").slick({
		dots: true,
		fade: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		autoplay: true,
		autoplaySpeed: 5000,
		prevArrow: ".js-prev",
		nextArrow: ".js-next",
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					dots: false
				}
			},
			{
				breakpoint: 768,
				settings: {
					dots: false,
					arrows: false
				}
			}
		]
	});

	$(".js-logo-slider").slick({
		infinite: false,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".js-items-slider").slick({
		dots: false,
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		prevArrow: ".js-prev-item",
		nextArrow: ".js-next-item",
		adaptiveheight: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
					centerPadding: '40px'
				}
			}
		  ]
	});

	$(".js-post-slider").slick({
		dots: false,
		slidesToShow: 4,
		slidesToScroll: 4,
		prevArrow: '<button class="slide-prev js-post-prev" type="button"><i class="i-icon-caret-l"></i></button>',
		nextArrow: '<button class="slide-next js-post-next" type="button"><i class="i-icon-caret-r"></i></button>',
		adaptiveheight: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
					centerPadding: '40px'
				}
			}
		  ]
	});
	$(".js-menu-slider").slick({
		dots: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		prevArrow: '<button class="slide-prev js-post-prev" type="button"><i class="i-icon-caret-l"></i></button>',
		nextArrow: '<button class="slide-next js-post-next" type="button"><i class="i-icon-caret-r"></i></button>',
		adaptiveheight: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
					centerPadding: '40px'
				}
			}
		  ]
	});
	$(".js-full-slider").slick({
		dots: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: false,
		prevArrow: '<button class="slide-prev js-post-prev" type="button"><i class="i-icon-prev"></i></button>',
		nextArrow: '<button class="slide-next js-post-next" type="button"><i class="i-icon-next"></i></button>',
	});
	$(".js-img-slider").slick({
		dots: false,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<button class="slide-prev js-post-prev" type="button"><i class="i-icon-caret-l"></i></button>',
		nextArrow: '<button class="slide-next js-post-next" type="button"><i class="i-icon-caret-r"></i></button>',
		adaptiveheight: true
	});
	$(".js-crsl-people").slick({
		dots: false,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<button class="slide-prev js-post-prev" type="button"><i class="i-icon-caret-l"></i></button>',
		nextArrow: '<button class="slide-next js-post-next" type="button"><i class="i-icon-caret-r"></i></button>',
		adaptiveheight: true
	});
	$(".js-crsl-gallery").slick({
		dots: false,
		infinite: true,
		slidesPerRow: 2,
		rows: 2,
		prevArrow: '<button class="slide-prev js-post-prev" type="button"><i class="i-icon-caret-l"></i></button>',
		nextArrow: '<button class="slide-next js-post-next" type="button"><i class="i-icon-caret-r"></i></button>'
	});

	$('.js-masonry').masonry({
		itemSelector: '.js-masonry-item',
		columnWidth: '.js-masonry-sizer',
		gutter: 0,
		percentPosition: true
	});

	var $datepicker = $(".js-datepicker");
	$datepicker.datepicker({
		dateFormat: "dd.mm.y"
	}).datepicker( $.datepicker.regional[ "ru" ] );

	$(".js-validate").validate({
		errorClass: "is-error",
		validClass: "is-success",
		errorElement: "span",
		rules: {
			email: {
				required: true,
				email: true
			}
		},
		messages: {
			company: {
				required: "Напишите имя компании"
			},
			password: {
				required: "Придумайте пароль не короче 6-ти символов",
				minlength: 6
			},
			password_again: {
			    equalTo: "#password"
			},
			email: {
				email: "Пожалуйста, введите корректный e-mail"
			},
			city: {
				required: "Пожалуйста, выберите город",
			}
		}
	});
	$.extend($.validator.messages, {
    	required: "Обязательное поле",
	});
	$(".js-fancybox a[rel]").fancybox({
		helpers : {
			overlay: {
				locked: false
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		},
		nextEffect: 'fade',
		prevEffect: 'fade',
		openEffect: 'fade',
		closeEffect: 'fade'
	});

	var scrollbar = $('.js-scrollbar');
	if (scrollbar.length) {
		scrollbar.perfectScrollbar(); 
	}
	 
});